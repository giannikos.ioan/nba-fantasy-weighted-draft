list_of_candidates = [
    'Taco Tuesday 🌮',
    'San Antonio Alpas',
    'Pathsa Mesie',
    'Luca Nowitzki',
    'Agrinio Canaries',
    'Melissourgoi Bees',
    'Aridea Bulls',
    'Nebula Blue Hawks',
    'Houston Rockets',
    'London Empire',
    'CopeNBAgen Mambas',
    'Venice Beach Jackboys',
    'High Voltage',
    'Göta Vikings'

]
number_of_items_to_pick = 14
probability_distribution = [
    0.009523809523809520,
    0.019047619047619000,
    0.028571428571428500,
    0.038095238095238100,
    0.047619047619047700,
    0.057142857142857100,
    0.066666666666666700,
    0.076190476190476300,
    0.085714285714285600,
    0.095238095238095200,
    0.104761904761905000,
    0.114285714285714000,
    0.123809523809524000,
    0.133333333333333000
]
