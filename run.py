import sys
import os
from termcolor import colored
from time import time
from numpy.random import choice

def main():
    list_of_candidates = []
    probability_distribution = []
    sum = 0
    number_of_items_to_pick = int(input('Enter the nuber of the teams: '))
    for i in range(1,number_of_items_to_pick+1):
        sum += i
    print(colored('Start by importing the team names!', 'blue'))
    for i in range(1,number_of_items_to_pick+1):
        if (i == 1):
            print('Enter Team with the absolutely best chance to win lottery.')
        elif (i == 2):
            print('Enter Team with ' + str(i) + 'nd best chance to win lottery.')
        else:
            print('Enter Team with '+ str(i) +'th best chance to win lottery.')
        ele = str(input())
        list_of_candidates.append(colored(ele, 'yellow'))
        jole = i/sum
        probability_distribution.append(jole)
    print(colored('Congratulations! The teams participating are: ', 'red'))
    for i in range(0,number_of_items_to_pick):
        print(list_of_candidates[i]+ ' with ' + str(round(probability_distribution[number_of_items_to_pick-i-1]*100, 2)) + '% chances of winning the lottery')

    input(colored('Let the draft begin! Press Enter to Start', 'red'))
    print('{}:'.format(time() * 1000) + colored(' Starting execution - Draw', 'blue'))
    draw = choice(list_of_candidates, number_of_items_to_pick,
                  p=probability_distribution, replace=False)
    for p in draw:
        print("Team " + colored(p, 'red') +" is awarded with the " + colored(str(number_of_items_to_pick), 'green') + " pick")
        input()
        number_of_items_to_pick -= 1
    print(colored('Enjoy your fantasy season <3', 'red'))

def terminate(msg):
    print(msg)
    try:
        sys.exit(0)
    except SystemExit:
        # noinspection PyProtectedMember
        os._exit(0)



if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        terminate('{}: Execution Interrupted'.format(time() * 1000))
